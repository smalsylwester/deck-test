package pl.adriankozlowski;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toList;

public class Main {

	public static final int DEFAULT_TILE_COUNT = 3;
	public static final int DEFAULT_TILE_SIZE = 13;

	public static void main(String[] args) {
		Main main = new Main();
		Collection<Card> cards = main.generateDeck();
		cards = main.shuffle(cards);
		Map<String, List<Card>> distributedCards = main.dealCards(cards);
	}

	public Collection<Card> shuffle(final Collection<Card> cards) {
		List<Card> newCardsToShuffle = cards.stream().map(Card::new).collect(toList());
		Dealer.shuffle(newCardsToShuffle);
		return newCardsToShuffle;
	}

	public Map<String, List<Card>> dealCards(Collection<Card> cards) {
		return Dealer.dealCards(cards, DEFAULT_TILE_COUNT, DEFAULT_TILE_SIZE);
	}

	public Collection<Card> generateDeck() {
		return Dealer.prepareDeck();
	}
}
