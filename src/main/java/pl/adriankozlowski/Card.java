package pl.adriankozlowski;

import java.util.Arrays;
import java.util.stream.Stream;

public class Card {
    private Value value;
    private Suit suit;

    public Card(){}

    public Card(Value value, Suit suit) {
        this.value = value;
        this.suit = suit;
    }

    public Card(Card card) {
        this.value = card.value;
        this.suit = card.suit;
    }

    public Value getValue() {
        return value;
    }

    public void setValue(Value value) {
        this.value = value;
    }

    public Suit getSuit() {
        return suit;
    }

    public void setSuit(Suit suit) {
        this.suit = suit;
    }

    public enum Value {
        TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING, ACE;

        public static Stream<Value> stream() {
            return Arrays.stream(values());
        }
    }

    public enum Suit {
        HEARTS, CLUBS, SPADES, DIAMONDS;

        public static  Stream<Suit> stream() {
            return Arrays.stream(values());
        }
    }
}
