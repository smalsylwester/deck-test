package pl.adriankozlowski;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;

public class Dealer {

	/**
	 * Shuffles {@link Card} collection. No return value - source collection is modified.
	 *
	 * @param cards {@link Card} collection to shuffle
	 */
	public static void shuffle(List<Card> cards) {
		Collections.shuffle(cards);
	}

	/**
	 * Prepares {@link Card} collection containing every combination
	 * of {@link Card.Suit} and {@link Card.Value}
	 *
	 * @return prepared {@link Card} collection
	 */
	public static Collection<Card> prepareDeck() {
		return Card.Suit.stream().flatMap(
					suit -> Card.Value.stream().map(
						value -> new Card(value, suit)))
				.collect(toList());
	}

	/**
	 * Deals {@link Card} collection into {@code tileCount} tiles of maximum size {@code tileSize}.
	 * If there is not enough cards in source collection result tile size is smaller.
	 * Removes dealt cards from source collection. Leaves cards that cannot be split evenly.
	 *
	 * @param cards     source {@link Card} collection to deal
	 * @param tileCount number of tiles to deal into
	 * @param tileSize  maximum number of cards in tile
	 * @return {code Map<String, List<Card>>} containing dealt cards - key represents tile name,
	 * value represent {@link Card} collection on this tile.
	 */
	public static Map<String, List<Card>> dealCards(Collection<Card> cards, int tileCount, int tileSize) {

		int cardsToDeal = Math.min(tileCount * tileSize, cards.size() - cards.size() % tileCount);
		final AtomicInteger cardPosition = new AtomicInteger();

		return cards.stream()
				.limit(cardsToDeal)
				.collect(toList())
				.stream()
				.peek(cards::remove)
				.collect(groupingBy(card -> getTileName(cardPosition.getAndIncrement(), tileCount)));
	}

	/**
	 * Calculates tile name to which Card with certain {@code cardPosition} should be dealt.
	 *
	 * @param cardPosition card position in dealt {@link Card}  collection
	 * @param tileCount    number of tiles we are dealing into
	 * @return tile name to which Card should be dealt
	 */
	private static String getTileName(int cardPosition, int tileCount) {
		int calculatedTileNumber = cardPosition % tileCount + 1;
		return "tile" + calculatedTileNumber;
	}
}
